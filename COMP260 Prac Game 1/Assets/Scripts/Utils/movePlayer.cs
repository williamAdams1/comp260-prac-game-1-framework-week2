﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayer : MonoBehaviour
{
    public float maxSpeed = 5.0f;
    public float acceleration = 1.0f;
    private float speed = 0.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;

    public float destroyRadius = 1.0f;
    private beeSpawnning beeSpawner;

    public GameObject player;
	// Use this for initialization
	void Start ()
    {

        beeSpawner = FindObjectOfType<beeSpawnning>();

    }

    // Update is called once per frame
    void Update () {
      

        if(player.tag == "Player")
        {
            playerMoveOne();
        }

      //  if (player.tag == "PLayer2")
        //{
         //   playerMoveTwo();
        //}

        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }
    }

    void playerMoveOne()
    {
        float turn = Input.GetAxis("Horizontal");


        float forward = Input.GetAxis("Vertical");


        move(forward,turn);
    }
    void playerMoveTwo()
    {

        float turn = Input.GetAxis("Horizontal2");


        float forward = Input.GetAxis("Vertical2");
        move(forward, turn);
       
    }

    void move(float forward, float turn)
    {

        

        if (forward > 0)
        {
            speed = speed + acceleration * Time.deltaTime;
           

                transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);
            

        }
        else if (forward < 0)
        {
            speed = speed - acceleration * Time.deltaTime;
            transform.Rotate(0, 0, turn * turnSpeed * speed * Time.deltaTime);
        }
        else
        {
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;

            }

            else
            {
                speed = 0;

            }
        }
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
        Vector2 velocity = Vector2.up * speed;
        transform.Translate(velocity * Time.deltaTime);
    }
}
