﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beeSpawnning : MonoBehaviour {

    public int nBee = 50;
    public moveBee beePrefab;
    public Rect spawnRect;
    public float beePeriod;
    public int beePeriodRan;
    
    
    public int beePeriodMIn;
    public int beePeriodMax;
    int time = 0;

    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    private float speed;
    private float turnSpeed;

    
    private Transform target;
    private Vector2 heading;
    // Use this for initialization
    void Start ()
    {
      
        movePlayer p = FindObjectOfType<movePlayer>();
        target = p.transform;
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
                                 Random.value);
         
        beePeriodRan = Random.Range(beePeriodMIn, beePeriodMax);
    
    }

    // Update is called once per frame
    void Update ()
    {/*
        beePeriod -= 1;

        if(beePeriod >= 0)
        {
            moveBee bee = Instantiate(beePrefab);
            bee.transform.parent = transform;
           

            float x = spawnRect.xMin +
                 Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
            beePeriod += sec;

        }*/


     

        while (beePeriod >= 0)
        {
            moveBee bee = Instantiate(beePrefab);
            bee.transform.parent = transform;


            float x = spawnRect.xMin +
                 Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
            beePeriodRan += beePeriodRan = Random.Range(beePeriodMIn, beePeriodMax);
            //time = 0;
            beePeriod =  beePeriod * Time.deltaTime;
        }
        
    }


    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);

            }
        }
    }

   

}
