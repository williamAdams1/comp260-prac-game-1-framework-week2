﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour {

    public float maxSpeed = 5.0f;
    public float acceleration = 1.0f;
    private float speed = 0.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;

 
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
      
        float turn = Input.GetAxis("Horizontal");
        

        float forward = Input.GetAxis("Vertical");

 
        if (forward > 0)
        {
            speed = speed + acceleration * Time.deltaTime;
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);
        }
        else if (forward < 0)
        {
            speed = speed - acceleration * Time.deltaTime;
            transform.Rotate(0, 0, -turn * turnSpeed * speed  * Time.deltaTime);
        }
          else
          {
              if(speed>0)
              {
                  speed = speed - brake * Time.deltaTime;
               
            }
             
              else
              {
                speed = 0;
               
            }
          }
         speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
        Vector2 velocity = Vector2.up * speed;
        transform.Translate(velocity * Time.deltaTime);

    }

   
}