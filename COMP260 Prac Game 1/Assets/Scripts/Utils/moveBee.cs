﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBee : MonoBehaviour {

    public float speed = 4.0f;
    public float turnSpeed = 180.0f;
    public Vector2 target;
    private Vector2 heading = Vector2.right;
    public Transform player1;
   public Transform player2;
    public ParticleSystem explosionPrefab;

    //Update is called once per frame
    void Update ()
    {
        target = Vector2.zero;
        Vector2 direction = player1.position - transform.position;
        Vector2 directionP2 = player2.position - transform.position;
        if (direction.magnitude < directionP2.magnitude)
        {
            target = direction;
        }
        else
        {
            target = directionP2;
        }
      
        float angle = turnSpeed * Time.deltaTime;
         
        if(target.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);

        }
        else
        {
            heading = heading.Rotate(-angle);
        }


        transform.Translate(heading * speed * Time.deltaTime);
	}
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = player1.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);

        Gizmos.color = Color.blue;
       Vector2 directionp2 = player2.position - transform.position;
        Gizmos.DrawRay(transform.position, directionp2);
    }

    void OnDestroy()
    {
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;

        Destroy(explosion.gameObject, explosion.duration);
    }
}
